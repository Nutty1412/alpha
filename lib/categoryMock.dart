class CategoryMock{
  var catagory = [
    {
      'name' : 'เสื้อแจ็คเก็ต dainese',
      'picture' : '1',
      'price' : '3,450',
      'category' : "เสื้อ"
    },
    {
      'name' : 'รองเท้า Pro Biker Speed ',
      'picture' : '9',
      'price' : '3,500',
      'category' : "รองเท้า"
    },
    {
      'name' : 'ถุงมือ Kawasaki',
      'picture' : '11',
      'price' : '640',
      'category' : "ถุงมือ"
    },
    {
      'name' : 'หมวกกันน็อค KYT ',
      'picture' : '15',
      'price' : '3,200',
      'category' : "หมวก"
    },
  ];
}