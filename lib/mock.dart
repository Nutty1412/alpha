class Mock{
  var catagory = [
    {
      'name' : 'เสื้อแจ็คเก็ต dainese',
      'picture' : '1',
      'price' : '3,450',
      'category' : "เสื้อ"
    },
    {
      'name' : 'เสื้อแจ็คเก็ต dainese',
      'picture' : '2',
      'price' : '3,650',
      'category' : "เสื้อ"
    },
    {
      'name' : 'เสื้อแจ็คเก็ต Alpinestars ',
      'picture' : '3',
      'price' : '7,290',
      'category' : "เสื้อ"
    },
    {
      'name' : 'เสื้อแจ็คเก็ต Alpinestars ',
      'picture' : '4',
      'price' : '8,650',
      'category' : "เสื้อ"
    },
    {
      'name' : 'เสื้อแจ็คเก็ต Alpinestars',
      'picture' : '5',
      'price' : '11,000',
      'category' : "เสื้อ"
    },
    {
      'name' : 'เสื้อแจ็คเก็ต RS Taichi',
      'picture' : '6',
      'price' : '6,550',
      'category' : "เสื้อ"
    },
    {
      'name' : 'ชุด Safety HELD ',
      'picture' : '7',
      'price' : '25,000',
      'category' : "เสื้อ"
    },
    {
      'name' : 'ชุด Safety HELD',
      'picture' : '8',
      'price' : '35,000',
      'category' : "เสื้อ"
    },
    {
      'name' : 'รองเท้า Pro Biker Speed ',
      'picture' : '9',
      'price' : '3,500',
      'category' : "รองเท้า"
    },
    {
      'name' : 'รองเท้า Augi AR-1',
      'picture' : '10',
      'price' : '4,900',
      'category' : "รองเท้า"
    },
    {
      'name' : 'ถุงมือ Kawasaki',
      'picture' : '11',
      'price' : '640',
      'category' : "ถุงมือ"
    },
    {
      'name' : 'ถุงมือ RS Taichi ',
      'picture' : '12',
      'price' : '890',
      'category' : "ถุงมือ"
    },
    {
      'name' : 'ถุงมือ SCOYCO MX54',
      'picture' : '13',
      'price' : '450',
      'category' : "ถุงมือ"
    },
    {
      'name' : 'ถุงมือ Alpinestars',
      'picture' : '14',
      'price' : '480',
      'category' : "ถุงมือ"
    },
    {
      'name' : 'หมวกกันน็อค KYT ',
      'picture' : '15',
      'price' : '3,200',
      'category' : "หมวก"
    },
    {
      'name' : 'หมวกกันน็อค KYT ',
      'picture' : '16',
      'price' : '4,200',
      'category' : "หมวก"
    },
    {
      'name' : 'หมวกกันน็อค AGV GLOSSY ',
      'picture' : '17',
      'price' : '40,00',
      'category' : "หมวก"
    },
    {
      'name' : 'หมวกกันน็อค Arai HAYDAN WSBK',
      'picture' : '18',
      'price' : '25,000',
      'category' : "หมวก"
    },
    {
      'name' : 'หมวกกันน็อค AGV K-1',
      'picture' : '19',
      'price' : '7,900',
      'category' : "หมวก"
    },
    {
      'name' : 'หมวกกันน็อค AGV K3SV ROSSI MISANO ',
      'picture' : '20',
      'price' : '8,600',
      'category' : "หมวก"
    },
  ];
}