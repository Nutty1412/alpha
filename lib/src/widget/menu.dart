import 'package:benelli/src/controllers/signIn.dart';
import 'package:benelli/src/widget/customLoading.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../App.dart';

class Menu extends StatefulWidget {
  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  String uid;
  Future<Null> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      uid = prefs.getString('uid');
    });
  }

  @override
  void initState() { 
    getToken();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: StreamBuilder(
        stream: Firestore.instance.collection("users").document(uid).snapshots(),
        builder: (context, AsyncSnapshot<DocumentSnapshot> snapshot) {
          if (snapshot.hasData) {
            var data = snapshot.data.data;
            return ListView(
              children: <Widget>[
                UserAccountsDrawerHeader(
                  accountName: Text(data['name']),
                  accountEmail: Text("Role : " + data['role']),
                  currentAccountPicture: CircleAvatar(
                    backgroundImage: NetworkImage(data['picture']),
                  ),
                ),
                ListTile(
                  title: Text("Logout"),
                  onTap: () async {
                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    prefs.remove("uid");
                    prefs.remove("role");
                    prefs.remove("name");
                    await Auth().signOutGoogle();
                    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => App()), (Route<dynamic> route) => false);
                  },
                )
              ],
            );
          } else {
            return CustomLoading();
          }
        },
      ),
    );
  }
}