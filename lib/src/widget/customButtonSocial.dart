import 'package:flutter/material.dart';

class CustomButtonSocial extends StatelessWidget {
  final String image;
  final VoidCallback onTap;

  CustomButtonSocial({@required this.image,this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: this.onTap,
      child: Container(
        child: Image.asset(this.image,height: 48,),
      ),
    );
  }
}