import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';

class Alert extends StatelessWidget {
  final AlertType alertType;
  final String title;
  final String message,submessage;
  final String btnText;
  final bool havesubmessage;
  final VoidCallback onPressed;

  Alert({this.alertType, @required this.title, @required this.message, @required this.btnText, this.havesubmessage = false, this.submessage = "", this.onPressed});

  @override
  Widget build(BuildContext context) {
    final Size deviceSize = MediaQuery.of(context).size;
    final double width = deviceSize.width;
    final double height = deviceSize.height;
    bool havecorrect;
    if (this.alertType.toString() == 'AlertType.correct'){
      havecorrect = true;
    } else {
      havecorrect = false;
    }

    return Dialog(
      backgroundColor: Colors.transparent,
      elevation: 0,
      child: Stack(
        children: <Widget>[
          Container(
            width: width * 1,
            height: havesubmessage ? height * 0.35 : height *0.3,
            margin: EdgeInsets.only(bottom: 45),
            decoration: BoxDecoration(
              color: Colors.white,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.all(
                Radius.circular(6.0),
              ),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  child: Container(
                    child: FlareActor('assets/animation/${this.alertType.toString().split(".")[1]}.flr',
                      animation:havecorrect ? 'Untitled' : 'Error',
                    ),
                    width: width*0.3,
                    height: width*0.3,
                  )
                ),
                Container(
                  child: Text(this.title,
                    style: TextStyle(
                      fontWeight: FontWeight.bold
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  child: Text(this.message,
                    textAlign: TextAlign.center
                  ),
                ),
                havesubmessage ? Text(submessage,style: TextStyle(color: Color(0xFFCF0807)),
                ) : Text(""),
              ],
            ),
          ),
          Positioned(
            bottom: 20,
            right: 50,
            left: 50,
            child: RaisedButton(
              color: Theme.of(context).primaryColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
              child: Text(this.btnText),
              onPressed: this.onPressed,
            ),
          ),
        ],
      ),
    );
  }
}

enum AlertType {
  correct,
  wrong
}