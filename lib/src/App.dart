import 'package:benelli/src/controllers/auth.dart';
import 'package:benelli/src/screen/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
            const Locale('en', 'US'),
            const Locale('th', 'TH'),
      ],
      title: 'Benelli',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.orange,
      ),
      routes: <String, WidgetBuilder> {
        '/home': (BuildContext context) => Home(),
      },
      home: AuthPage(),
    );
  }
}