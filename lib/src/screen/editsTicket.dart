import 'package:benelli/src/widget/alert.dart';
import 'package:benelli/src/widget/customLoading.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';

class EditsTicket extends StatefulWidget {
  final caseID;
  final documendID;

  EditsTicket({
    @required this.caseID,
    @required this.documendID
  });

  @override
  _EditsTicketState createState() => _EditsTicketState();
}

class _EditsTicketState extends State<EditsTicket> {

  static GlobalKey<FormState> _formKey;
  String statusValue = 'ลูกค้ายืนยันการเข้ารับบริการ';
  String technicain,dateTime,time,details;
  String dateTimeUpdate,timeUpdate;
  bool createForm = false;
  bool canEdit = true;

  @override
  void initState() {
    _formKey = GlobalKey<FormState>();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        appBar: AppBar(
          title: Text("แก้ไขรายละเอียดการแจ้งซ่อม"),
        ),
        body: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                children: <Widget>[
                  StreamBuilder(
                    stream: Firestore.instance.collection("ticket").document(this.widget.documendID).snapshots(),
                    builder: (context,snapshot) {
                      if (snapshot.hasData) {
                        dateTime = snapshot.data['dateTime'];
                        time = snapshot.data['time'];
                        details = snapshot.data['details'];
                        return Column(
                          children: <Widget>[
                            container(
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  detail(
                                    "สถานะดำเนินการ",
                                    DropdownButton<String>(
                                      value: statusValue,
                                      onChanged: (String newValue) {
                                        setState(() {
                                          statusValue = newValue;
                                        });
                                      },
                                      items: <String>['ลูกค้ายืนยันการเข้ารับบริการ','ลูกค้ายกเลิกการเข้ารับบริการ'].map<DropdownMenuItem<String>>((String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(
                                            value,
                                          ),
                                        );
                                      }).toList(),
                                    )
                                  ),
                                ],
                              ),
                            ),
                            if (statusValue == 'ลูกค้ายืนยันการเข้ารับบริการ')
                            Padding(
                              padding: EdgeInsets.only(top: 20),
                              child: Column(
                                children: <Widget>[
                                  StreamBuilder<QuerySnapshot>(
                                    stream : Firestore.instance.collection("users").where('role',isEqualTo: 'Technician').snapshots(),
                                    builder: (context, snapshot) {
                                      if (snapshot.hasData) {
                                        return container(
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              detail("ช่างที่ได้รับมอบหมาย", DropdownButton(
                                                value: technicain,
                                                onChanged: (String newValue) {
                                                  setState(() {
                                                    technicain = newValue;
                                                  });
                                                },
                                                items: snapshot.data.documents.map((DocumentSnapshot document) {
                                                  return DropdownMenuItem<String>(
                                                    value: document.data['name'],
                                                    child: Text(
                                                      document.data['name']
                                                    ),
                                                  );
                                                }).toList(),
                                              ))
                                            ],
                                          ),
                                        );
                                      }
                                      return CustomLoading();
                                    },
                                  ),
                                ],
                              )
                            ),
                          if (technicain != null && statusValue == 'ลูกค้ายืนยันการเข้ารับบริการ')
                          Padding(
                            padding: EdgeInsets.only(top: 20),
                            child: container(
                              detail("วันที่ลูกค้าเข้ารับบริการ", RaisedButton(
                                onPressed: () async {
                                  DateTime newDateTime = await showRoundedDatePicker(
                                    theme: ThemeData(
                                      primaryColor: Theme.of(context).primaryColor,
                                      accentColor: Theme.of(context).primaryColor
                                    ),
                                    context: context,
                                    era: EraMode.BUDDHIST_YEAR,
                                    initialDate: DateTime.now(),
                                    firstDate: DateTime.now().subtract(Duration(days: 3)),
                                    lastDate: DateTime.now().add(Duration(days: 30)),
                                  );
                                  if (newDateTime != null) {
                                    setState(() => dateTimeUpdate = formatDate(newDateTime, [dd,'/', mm,'/', yyyy]).toString());
                                  }
                                },
                                color: Theme.of(context).primaryColor,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                child: Text(dateTimeUpdate == null ? dateTime : dateTimeUpdate),
                                ),
                              )
                            ),
                          ),
                          if (dateTime != null && technicain != null && statusValue == 'ลูกค้ายืนยันการเข้ารับบริการ') 
                          Padding(
                            padding: EdgeInsets.only(top: 20),
                            child: container(
                              detail("เวลาที่ลูกค้าเข้ารับบริการ", RaisedButton(
                                color: Theme.of(context).primaryColor,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                onPressed: () async {
                                  TimeOfDay timePicked = await showTimePicker(
                                    context: context,
                                    initialTime: TimeOfDay.now(),
                                  );
                                  if (timePicked != null) {
                                    setState(() => timeUpdate = timePicked.hour.toString() + " : " + timePicked.minute.toString());
                                  }
                                },
                                child: Text(timeUpdate == null ? time : timeUpdate),
                                ),
                              )
                            ),
                          ),
                          if (time != null && technicain != null && statusValue == 'ลูกค้ายืนยันการเข้ารับบริการ')
                          Padding(
                            padding: EdgeInsets.only(top: 20),
                            child: Column(
                              children: <Widget>[
                                container(
                                  detail("รายละเอียด", TextFormField(
                                    initialValue: details,
                                    onSaved: (String value) {
                                      details = value;
                                    },
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'กรุณากรอกข้อมูล';
                                      }
                                      return null;
                                    },
                                  ))
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 20),
                                  child: createForm ? CustomLoading() : RaisedButton(
                                    onPressed: () async {
                                      _formKey.currentState.validate();
                                      _formKey.currentState.save();
                                      setState(() {
                                        createForm = true;
                                      });
                                      await Firestore.instance.collection("ticket").document(this.widget.documendID).updateData({
                                        'technician' : technicain,
                                        'statusProgress' : statusValue,
                                        'details' : details,
                                        'dateTime' : dateTime,
                                        'time' : time,
                                        'updateDate' : DateTime.now()
                                      });
                                      setState(() {
                                        createForm = false;
                                      });
                                      await showDialog(
                                        context: context,
                                        builder: (context) {
                                          return Alert(
                                            alertType: AlertType.correct,
                                            title: "สำเร็จ",
                                            message: "แก้ไขรายละเอียดการแจ้งซ่อมเรียบร้อยแล้ว",
                                            btnText: 'ตกลง',
                                            onPressed: () {
                                              Navigator.of(context).pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
                                            },
                                          );
                                        }
                                      );
                                    },
                                    color: Theme.of(context).primaryColor,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                    ),
                                    child: Text("ยืนยัน"),
                                  ),
                                ),
                              ]
                            )
                          ),
                          if (statusValue == 'ลูกค้ายกเลิกการเข้ารับบริการ')
                          Padding(
                            padding: EdgeInsets.only(top: 20),
                            child: createForm ? CustomLoading() : RaisedButton(
                              onPressed: () async {
                                setState(() {
                                  createForm = true;
                                });
                                await Firestore.instance.collection("ticket").document(this.widget.documendID).updateData({
                                  'status' :'close',
                                  'statusProgress' : statusValue,
                                  'updateDate' : DateTime.now()
                                });
                                setState(() {
                                  createForm = false;
                                });
                                await showDialog(
                                  context: context,
                                  builder: (context) {
                                    return Alert(
                                      alertType: AlertType.correct,
                                      title: "สำเร็จ",
                                      message: "แก้ไขรายละเอียดการแจ้งซ่อมเรียบร้อยแล้ว",
                                      btnText: 'ตกลง',
                                      onPressed: () {
                                        Navigator.of(context).pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
                                      },
                                    );
                                  }
                                );
                              },
                              color: Theme.of(context).primaryColor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              child: Text("ยืนยัน"),
                            ),
                          )
                        ]);
                      }
                      return Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: Column(
                          children: <Widget>[
                            CustomLoading(),
                          ],
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget detail(title,Widget subTitle){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(title,style: TextStyle(fontWeight: FontWeight.bold,color: Theme.of(context).primaryColor)),
        subTitle
      ],
    );
  }

  Widget container(Widget child) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(
          color: Colors.grey
        )
      ),
      child: Padding(
        padding: EdgeInsets.all(10),
        child : child
      )
    );
  }
}