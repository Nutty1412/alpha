import 'package:benelli/src/screen/openTicketTech.dart';
import 'package:flutter/material.dart';

import 'closeTicketTech.dart';

class ScreenTechnician extends StatefulWidget {
  final String name;

  ScreenTechnician({
    @required this.name
  });

  @override
  _ScreenTechnicianState createState() => _ScreenTechnicianState();
}

class _ScreenTechnicianState extends State<ScreenTechnician> with TickerProviderStateMixin {
  TabController _customTabController;

  @override
  void initState() {
    super.initState();
    _customTabController = new TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _customTabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        TabBar(
          controller: _customTabController,
          indicatorColor: Theme.of(context).primaryColor,
          labelColor: Theme.of(context).primaryColor,
          unselectedLabelColor: Colors.black54,
          isScrollable: true,
          tabs: <Widget>[
            Tab(
              text: "รายการที่ดำเนินการอยู่",
            ),
            Tab(
              text: "รายการที่เสร็จสิ้นแล้ว",
            ),
          ],
        ),
        Container(
          height: screenHeight * 0.70,
          margin: EdgeInsets.only(left: 16.0, right: 16.0),
          child: TabBarView(
            controller: _customTabController,
            children: <Widget>[
              OpenTicketTech(
                name: this.widget.name
              ),
              CloseTicketTech(
                name: this.widget.name
              )
            ],
          ),
        )
      ],
    );
  }
}