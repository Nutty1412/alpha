import 'package:benelli/src/screen/screenAdmin.dart';
import 'package:benelli/src/screen/screenTechnician.dart';
import 'package:benelli/src/screen/screenUser.dart';
import 'package:benelli/src/widget/customLoading.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AskForHelp extends StatefulWidget {
  @override
  _AskForHelpState createState() => _AskForHelpState();
}

class _AskForHelpState extends State<AskForHelp> {
  String uid,role,name;
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getToken(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          return role == 'Member' ? ScreenUser(uid: uid) : role == 'Admin' ? ScreenAdmin() : ScreenTechnician(name: name);
        } else {
          return Center(
            child: CustomLoading(),
          );
        }
      }
    );
  }

  getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      uid = prefs.getString('uid');
      role = prefs.getString('role');
      name = prefs.getString('name');
    });

    return role;
  }
}