import 'package:benelli/src/widget/alert.dart';
import 'package:benelli/src/widget/customLoading.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class EditsTicketTech extends StatefulWidget {
  final caseID;
  final documendID;

  EditsTicketTech({
    @required this.caseID,
    @required this.documendID
  });

  @override
  _EditsTicketTechState createState() => _EditsTicketTechState();
}

class _EditsTicketTechState extends State<EditsTicketTech> {

  static GlobalKey<FormState> _formKey;
  String statusValue = 'ลูกค้านำรถเข้ามาซ่อมที่ศูนย์';
  String price;
  bool createForm = false;
  bool canEdit = true;

  @override
  void initState() { 
    _formKey = GlobalKey<FormState>();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        appBar: AppBar(
          title: Text("แก้ไขรายละเอียดการแจ้งซ่อม"),
        ),
        body: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                children: <Widget>[
                  StreamBuilder(
                    stream: Firestore.instance.collection("ticket").document(this.widget.documendID).snapshots(),
                    builder: (context,snapshot) {
                      if (snapshot.hasData) {
                        price = snapshot.data['price'];
                        return Column(
                          children: <Widget>[
                            container(
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  detail(
                                    "สถานะดำเนินการ",
                                    DropdownButton<String>(
                                      value: statusValue,
                                      onChanged: (String newValue) {
                                        setState(() {
                                          statusValue = newValue;
                                        });
                                      },
                                      items: <String>['ลูกค้านำรถเข้ามาซ่อมที่ศูนย์','ลูกค้ายกเลิกการเข้ารับบริการ'].map<DropdownMenuItem<String>>((String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(
                                            value,
                                          ),
                                        );
                                      }).toList(),
                                    )
                                  ),
                                ],
                              ),
                            ),
                          if (statusValue == 'ลูกค้านำรถเข้ามาซ่อมที่ศูนย์')
                          Padding(
                            padding: EdgeInsets.only(top: 20),
                            child: Column(
                              children: <Widget>[
                                container(
                                  detail("ราคาประเมิน", TextFormField(
                                    keyboardType: TextInputType.number,
                                    initialValue: price,
                                    onSaved: (String value) {
                                      price = value;
                                    },
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'กรุณากรอกข้อมูล';
                                      }
                                      return null;
                                    },
                                  ))
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 20),
                                  child: createForm ? CustomLoading() : RaisedButton(
                                    color: Theme.of(context).primaryColor,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                    ),
                                    onPressed: () async {
                                      _formKey.currentState.validate();
                                      _formKey.currentState.save();
                                      setState(() {
                                        createForm = true;
                                      });
                                      Firestore.instance.collection("ticket").document(this.widget.documendID).updateData({
                                        'price' : price,
                                        'statusProgress' : statusValue,
                                        'updateDate' : DateTime.now()
                                      });
                                      setState(() {
                                        createForm = false;
                                      });
                                      await showDialog(
                                        context: context,
                                        builder: (context) {
                                          return Alert(
                                            alertType: AlertType.correct,
                                            title: "สำเร็จ",
                                            message: "แก้ไขรายละเอียดการแจ้งซ่อมเรียบร้อยแล้ว",
                                            btnText: 'ตกลง',
                                            onPressed: () {
                                              Navigator.of(context).pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
                                            },
                                          );
                                        }
                                      );
                                    },
                                    child: Text("ยืนยัน"),
                                  ),
                                ),
                              ]
                            )
                          ),
                            if (statusValue == 'ลูกค้ายกเลิกการเข้ารับบริการ')
                            Padding(
                              padding: EdgeInsets.only(top: 20),
                              child: createForm ? CustomLoading() : RaisedButton(
                                onPressed: () async {
                                  setState(() {
                                    createForm = true;
                                  });
                                  await Firestore.instance.collection("ticket").document(this.widget.documendID).updateData({
                                    'status' :'close',
                                    'statusProgress' : statusValue,
                                    'updateDate' : DateTime.now()
                                  });
                                  setState(() {
                                    createForm = false;
                                  });
                                  await showDialog(
                                    context: context,
                                    builder: (context) {
                                      return Alert(
                                        alertType: AlertType.correct,
                                        title: "สำเร็จ",
                                        message: "แก้ไขรายละเอียดการแจ้งซ่อมเรียบร้อยแล้ว",
                                        btnText: 'ตกลง',
                                        onPressed: () {
                                          Navigator.of(context).pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
                                        },
                                      );
                                    }
                                  );
                                },
                                color: Theme.of(context).primaryColor,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                child: Text("ยืนยัน"),
                              ),
                            )
                          ]
                        );
                      }
                      return Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: Column(
                          children: <Widget>[
                            CustomLoading(),
                          ],
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget detail(title,Widget subTitle){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(title,style: TextStyle(fontWeight: FontWeight.bold,color: Theme.of(context).primaryColor)),
        subTitle
      ],
    );
  }

  Widget container(Widget child) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(
          color: Colors.grey
        )
      ),
      child: Padding(
        padding: EdgeInsets.all(10),
        child : child
      )
    );
  }
}