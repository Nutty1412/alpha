import 'package:benelli/src/widget/customLoading.dart';
import 'package:flutter/material.dart';

import '../../mock.dart';

class Product extends StatefulWidget {
  final category;
  Product({
    @required this.category
  });
  @override
  _ProductState createState() => _ProductState();
}

class _ProductState extends State<Product> {
  @override
  void initState() {
    checkCategoryUser();
    super.initState();
  }
  var categoryUserSelect = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(this.widget.category),
        centerTitle: true,
      ),
      body: categoryUserSelect == [] ? Center(
        child: CustomLoading(),
      ) : Padding(
        padding: EdgeInsets.all(8.0),
        child: GridView(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2
          ),
          children: List.generate(categoryUserSelect.length, (index) {
            var data = categoryUserSelect[index];
            return Stack(
              fit: StackFit.expand,
              children: [
                Image.asset(
                  'assets/images/${data['picture']}.png',
                  fit: BoxFit.cover,
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        color: Colors.white,
                        child: Text(data['name'])
                      ),
                      Container(
                        color: Colors.white,
                        child: Text('ราคา' + data['price'] + 'บาท')
                      )
                    ],
                  ),
                )
              ],
            );
          })
        ),
      )
    );
  }

  checkCategoryUser(){
    for (int i = 0 ; i < Mock().catagory.length ; i++) {
      if (Mock().catagory[i]['category'] == this.widget.category) {
        categoryUserSelect.add(Mock().catagory[i]);
      }
    }
    setState(() {});
  }
}