import 'package:benelli/src/screen/askForHelp.dart';
import 'package:benelli/src/screen/category.dart';
import 'package:benelli/src/screen/trip/manageTrip.dart';
import 'package:benelli/src/widget/menu.dart';
import 'package:flutter/material.dart';

import 'tripUserPage.dart';
class Home extends StatefulWidget {

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _selectedIndex = 0;
  var _widgetOptions = <Widget>[
    AskForHelp(),
    ManageTrip(),
    Catagory()
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Benelli'),
      ),
      drawer: Menu(),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'หน้าหลัก',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.trip_origin),
            label: 'ทริป',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.category),
            label: 'แคตตาล็อกสินค้า',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }
}