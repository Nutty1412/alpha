import 'package:benelli/src/screen/detailTicketTech.dart';
import 'package:benelli/src/widget/customLoading.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class CloseTicketTech extends StatefulWidget {
  final String name;
  CloseTicketTech({
    @required this.name
  });

  @override
  _CloseTicketTechState createState() => _CloseTicketTechState();
}

class _CloseTicketTechState extends State<CloseTicketTech> with AutomaticKeepAliveClientMixin<CloseTicketTech>{
  bool isExpanded = true;
  bool isExpanded2 = true;
  @override
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return ListView(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey,
            ),
            borderRadius: BorderRadius.circular(10)
          ),
          child: ExpansionTile(
            initiallyExpanded: true,
            trailing: Icon(isExpanded ? Icons.expand_less : Icons.expand_more,color: isExpanded ? Theme.of(context).primaryColor : Colors.black),
            onExpansionChanged: (bool expanding) => setState(() => this.isExpanded = expanding),
            title: Text(
              "ดำเนินการซ่อมเสร็จสิ้น",
              style: TextStyle(
                color: isExpanded ? Theme.of(context).primaryColor : Colors.black,
                fontWeight: FontWeight.bold
              ),
            ),
            children: <Widget>[
              StreamBuilder(
                stream : Firestore.instance.collection("ticket").where('technician',isEqualTo: this.widget.name).where('status',isEqualTo: 'close').where('statusProgress',isEqualTo: 'ดำเนินการซ่อมเสร็จสิ้น').snapshots(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Column(
                      children: List.generate(snapshot.data.documents.length,(i){
                        return Padding(
                          padding: EdgeInsets.only(bottom: 10,left: 10,right: 10),
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.grey[300],
                              ),
                              borderRadius: BorderRadius.circular(10)
                            ),
                            child: ListTile(
                              onTap: (){
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailTicketTech(
                                  documendID: snapshot.data.documents[i].documentID
                                )));
                              },
                              leading: CircleAvatar(
                                backgroundImage: NetworkImage(snapshot.data.documents[i]['userPhoto']),
                              ),
                              title: Text(snapshot.data.documents[i]['model']),
                              subtitle: Text(snapshot.data.documents[i]['statusProgress']),
                              trailing: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("คุณ : " + snapshot.data.documents[i]['name']),
                                  Text(snapshot.data.documents[i]['tel']),
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                    );
                  } else {
                    return Column(
                      children: <Widget>[
                        CustomLoading(),
                      ],
                    );
                  }
                }
              ),
            ],
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey,
            ),
            borderRadius: BorderRadius.circular(10)
          ),
          child: ExpansionTile(
            initiallyExpanded: true,
            trailing: Icon(isExpanded2 ? Icons.expand_less : Icons.expand_more,color: isExpanded2 ? Theme.of(context).primaryColor : Colors.black),
            onExpansionChanged: (bool expanding) => setState(() => this.isExpanded2 = expanding),
            title: Text(
              "ลูกค้ายกเลิกการเข้ารับบริการ",
              style: TextStyle(
                color: isExpanded2 ? Theme.of(context).primaryColor : Colors.black,
                fontWeight: FontWeight.bold
              ),
            ),
            children: <Widget>[
              StreamBuilder(
                stream : Firestore.instance.collection("ticket").where('technician',isEqualTo: this.widget.name).where('status',isEqualTo: 'close').where('statusProgress',isEqualTo: 'ลูกค้ายกเลิกการเข้ารับบริการ').snapshots(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Column(
                      children: List.generate(snapshot.data.documents.length,(i){
                        return Padding(
                          padding: EdgeInsets.only(bottom: 10,left: 10,right: 10),
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.grey[300],
                              ),
                              borderRadius: BorderRadius.circular(10)
                            ),
                            child: ListTile(
                              onTap: (){
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailTicketTech(
                                  documendID: snapshot.data.documents[i].documentID
                                )));
                              },
                              leading: CircleAvatar(
                                backgroundImage: NetworkImage(snapshot.data.documents[i]['userPhoto']),
                              ),
                              title: Text(snapshot.data.documents[i]['model']),
                              subtitle: Text(snapshot.data.documents[i]['statusProgress']),
                              trailing: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("คุณ : " + snapshot.data.documents[i]['name']),
                                  Text(snapshot.data.documents[i]['tel']),
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                    );
                  } else {
                    return Column(
                      children: <Widget>[
                        CustomLoading(),
                      ],
                    );
                  }
                }
              ),
            ],
          ),
        ),
      ],
    );
  }
}