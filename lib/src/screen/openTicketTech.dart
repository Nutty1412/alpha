import 'package:benelli/src/screen/detailTicketTech.dart';
import 'package:benelli/src/widget/customLoading.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class OpenTicketTech extends StatefulWidget {
  final String name;

  OpenTicketTech({
    @required this.name
  });

  @override
  _OpenTicketTechState createState() => _OpenTicketTechState();
}

class _OpenTicketTechState extends State<OpenTicketTech> with AutomaticKeepAliveClientMixin<OpenTicketTech>{
  bool isExpanded2 = true;
  bool isExpanded3 = true;
  @override
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return ListView(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey,
            ),
            borderRadius: BorderRadius.circular(10)
          ),
          child: ExpansionTile(
            initiallyExpanded: true,
            trailing: Icon(isExpanded2 ? Icons.expand_less : Icons.expand_more,color: isExpanded2 ? Theme.of(context).primaryColor : Colors.black),
            onExpansionChanged: (bool expanding) => setState(() => this.isExpanded2 = expanding),
            title: Text(
              "ลูกค้ายืนยันการเข้ารับบริการ",
              style: TextStyle(
                color: isExpanded2 ? Theme.of(context).primaryColor : Colors.black,
                fontWeight: FontWeight.bold
              ),
            ),
            children: <Widget>[
              StreamBuilder(
                stream : Firestore.instance.collection("ticket").where('technician',isEqualTo: this.widget.name).where('status',isEqualTo: 'open').where('statusProgress',isEqualTo: 'ลูกค้ายืนยันการเข้ารับบริการ').snapshots(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data.documents.length == 0) {
                      return Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Text("ไม่มีข้อมูล"),
                      );
                    }
                    return Column(
                      children: List.generate(snapshot.data.documents.length,(i){
                        return Padding(
                          padding: EdgeInsets.only(bottom: 10,left: 10,right: 10),
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.grey[300],
                              ),
                              borderRadius: BorderRadius.circular(10)
                            ),
                            child: ListTile(
                              onTap: (){
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailTicketTech(
                                  documendID: snapshot.data.documents[i].documentID
                                )));
                              },
                              leading: CircleAvatar(
                                backgroundImage: NetworkImage(snapshot.data.documents[i]['userPhoto']),
                              ),
                              title: Text(snapshot.data.documents[i]['model']),
                              subtitle: Text(snapshot.data.documents[i]['statusProgress']),
                              trailing: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("คุณ : " + snapshot.data.documents[i]['name']),
                                  Text(snapshot.data.documents[i]['tel']),
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                    );
                  } else {
                    return Column(
                      children: <Widget>[
                        CustomLoading(),
                      ],
                    );
                  }
                }
              ),
            ],
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey,
            ),
            borderRadius: BorderRadius.circular(10)
          ),
          child: ExpansionTile(
            initiallyExpanded: true,
            trailing: Icon(isExpanded3 ? Icons.expand_less : Icons.expand_more,color: isExpanded3 ? Theme.of(context).primaryColor : Colors.black),
            onExpansionChanged: (bool expanding) => setState(() => this.isExpanded3 = expanding),
            title: Text(
              "ลูกค้านำรถเข้ามาซ่อมที่ศูนย์",
              style: TextStyle(
                color: isExpanded3 ? Theme.of(context).primaryColor : Colors.black,
                fontWeight: FontWeight.bold
              ),
            ),
            children: <Widget>[
              StreamBuilder(
                stream : Firestore.instance.collection("ticket").where('technician',isEqualTo: this.widget.name).where('status',isEqualTo: 'open').where('statusProgress',isEqualTo: 'ลูกค้านำรถเข้ามาซ่อมที่ศูนย์').snapshots(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data.documents.length == 0) {
                      return Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Text("ไม่มีข้อมูล"),
                      );
                    }
                    return Column(
                      children: List.generate(snapshot.data.documents.length,(i){
                        return Padding(
                          padding: EdgeInsets.only(bottom: 10,left: 10,right: 10),
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.grey[300],
                              ),
                              borderRadius: BorderRadius.circular(10)
                            ),
                            child: ListTile(
                              onTap: (){
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailTicketTech(
                                  documendID: snapshot.data.documents[i].documentID
                                )));
                              },
                              leading: CircleAvatar(
                                backgroundImage: NetworkImage(snapshot.data.documents[i]['userPhoto']),
                              ),
                              title: Text(snapshot.data.documents[i]['model']),
                              subtitle: Text(snapshot.data.documents[i]['statusProgress']),
                              trailing: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("คุณ : " + snapshot.data.documents[i]['name']),
                                  Text(snapshot.data.documents[i]['tel']),
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                    );
                  } else {
                    return Column(
                      children: <Widget>[
                        CustomLoading(),
                      ],
                    );
                  }
                }
              ),
            ],
          ),
        ),
      ],
    );
  }
}