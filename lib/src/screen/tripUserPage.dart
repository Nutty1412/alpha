import 'package:benelli/src/screen/trip/closeTrip.dart';
import 'package:flutter/material.dart';

import 'trip/openTrip.dart';
import 'trip/userTrip.dart';

class TripUserPage extends StatefulWidget {
  @override
  _TripUserPageState createState() => _TripUserPageState();
}

class _TripUserPageState extends State<TripUserPage> with TickerProviderStateMixin {
  TabController _customTabController;

  @override
  void initState() {
    super.initState();
    _customTabController = new TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _customTabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        TabBar(
          controller: _customTabController,
          indicatorColor: Theme.of(context).primaryColor,
          labelColor: Theme.of(context).primaryColor,
          unselectedLabelColor: Colors.black54,
          isScrollable: true,
          tabs: <Widget>[
            Tab(
              text: "ทริปปัจจุบัน",
            ),
            Tab(
              text: "ทริปก่อนหน้านี้",
            ),
            Tab(
              text: "ทริปที่ทำการจอง",
            ),
          ],
        ),
        Container(
          height: screenHeight * 0.70,
          margin: EdgeInsets.only(left: 16.0, right: 16.0),
          child: TabBarView(
            controller: _customTabController,
            children: <Widget>[
              OpenTrip(),
              CloseTrip(),
              UserTrip()
            ],
          ),
        )
      ],
    );
  }
}