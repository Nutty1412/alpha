import 'package:benelli/src/widget/alert.dart';
import 'package:benelli/src/widget/customLoading.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class EditTicket extends StatefulWidget {
  final caseID;
  final documendID;
  final roleTech;

  EditTicket({
    @required this.caseID,
    @required this.documendID,
    this.roleTech = false
  });

  @override
  _EditTicketState createState() => _EditTicketState();
}

class _EditTicketState extends State<EditTicket> {
  static GlobalKey<FormState> _formKey;
  String statusValue = 'รอการติดต่อกลับจาก Admin';
  String technicain,details,talent;
  bool createForm = false;
  bool canEdit = true;

  @override
  void initState() { 
    _formKey = GlobalKey<FormState>();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        appBar: AppBar(
          title: Text("แก้ไขรายละเอียดการแจ้งซ่อม"),
        ),
        body: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                children: <Widget>[
                  container(
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        detail(
                          "สถานะดำเนินการ",
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              IconButton(
                                icon: Icon(Icons.check),
                                color: statusValue == 'ลูกค้ายืนยันการเข้ารับบริการ' ? Colors.green : Colors.grey,
                                onPressed: (){
                                  setState(() {
                                    statusValue = 'ลูกค้ายืนยันการเข้ารับบริการ';
                                  });
                                }
                              ),
                              IconButton(
                                icon: Icon(Icons.close),
                                color: statusValue == 'ลูกค้ายกเลิกการเข้ารับบริการ' ? Colors.red : Colors.grey,
                                onPressed: (){
                                  setState(() {
                                    statusValue = 'ลูกค้ายกเลิกการเข้ารับบริการ';
                                  });
                                }
                              )
                            ],
                          )
                        ),
                      ],
                    ),
                  ),
                  if (statusValue == 'ลูกค้ายืนยันการเข้ารับบริการ')
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: Column(
                      children: <Widget>[
                        StreamBuilder<QuerySnapshot>(
                          stream : Firestore.instance.collection("users").where('role',isEqualTo: 'Technician').snapshots(),
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              return container(
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    detail("ช่างที่ได้รับมอบหมาย", DropdownButton(
                                      value: technicain,
                                      onChanged: (String newValue) {
                                        setState(() {
                                          technicain = newValue;
                                        });
                                      },
                                      items: snapshot.data.documents.map((DocumentSnapshot document) {
                                        return DropdownMenuItem<String>(
                                          value: document.data['name'],
                                          child: Text(
                                            document.data['name']
                                          ),
                                        );
                                      }).toList(),
                                    ))
                                  ],
                                ),
                              );
                            }
                            return CustomLoading();
                          },
                        ),
                        if (technicain != null && statusValue == 'ลูกค้ายืนยันการเข้ารับบริการ')
                        Padding(
                          padding: EdgeInsets.only(top: 20),
                          child: Column(
                            children: <Widget>[
                              container(
                                detail("รายละเอียด", TextFormField(
                                  onSaved: (String value) {
                                    details = value;
                                  },
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'กรุณากรอกข้อมูล';
                                    }
                                    return null;
                                  },
                                ))
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 20),
                                child: createForm ? CustomLoading() : RaisedButton(
                                  color: Theme.of(context).primaryColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                  ),
                                  onPressed: () async {
                                    _formKey.currentState.validate();
                                    _formKey.currentState.save();
                                    setState(() {
                                      createForm = true;
                                    });
                                    await Firestore.instance.collection("ticket").document(this.widget.documendID).updateData({
                                      'status' :'open',
                                      'technician' : technicain,
                                      'statusProgress' : statusValue,
                                      'details' : details,
                                      'updateDate' : DateTime.now()
                                    });
                                    setState(() {
                                      createForm = false;
                                    });
                                    await showDialog(
                                      context: context,
                                      builder: (context) {
                                        return Alert(
                                          alertType: AlertType.correct,
                                          title: "สำเร็จ",
                                          message: "แก้ไขรายละเอียดการแจ้งซ่อมเรียบร้อยแล้ว",
                                          btnText: 'ตกลง',
                                          onPressed: () {
                                            Navigator.of(context).pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
                                          },
                                        );
                                      }
                                    );
                                  },
                                  child: Text("ยืนยัน"),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  if (statusValue == 'ลูกค้ายกเลิกการเข้ารับบริการ')
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: createForm ? CustomLoading() : RaisedButton(
                      onPressed: () async {
                        setState(() {
                          createForm = true;
                        });
                        await Firestore.instance.collection("ticket").document(this.widget.documendID).updateData({
                          'status' :'close',
                          'statusProgress' : statusValue,
                          'updateDate' : DateTime.now()
                        });
                        setState(() {
                          createForm = false;
                        });
                        await showDialog(
                          context: context,
                          builder: (context) {
                            return Alert(
                              alertType: AlertType.correct,
                              title: "สำเร็จ",
                              message: "แก้ไขรายละเอียดการแจ้งซ่อมเรียบร้อยแล้ว",
                              btnText: 'ตกลง',
                              onPressed: () {
                                Navigator.of(context).pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
                              },
                            );
                          }
                        );
                      },
                      color: Theme.of(context).primaryColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      child: Text("ยืนยัน"),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget detail(title,Widget subTitle){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(title,style: TextStyle(fontWeight: FontWeight.bold,color: Theme.of(context).primaryColor)),
        subTitle
      ],
    );
  }

  Widget container(Widget child) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(
          color: Colors.grey
        )
      ),
      child: Padding(
        padding: EdgeInsets.all(10),
        child : child
      )
    );
  }
}