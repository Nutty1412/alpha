import 'dart:io';

import 'package:benelli/src/widget/alert.dart';
import 'package:benelli/src/widget/customLoading.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as p;

import 'formHelp.dart';

class ScreenUser extends StatefulWidget {

  final String uid;
  ScreenUser({
    @required this.uid
  });

  @override
  _ScreenUserState createState() => _ScreenUserState();
}

class _ScreenUserState extends State<ScreenUser> {
  File file;
  String fileName = '';
  bool createForm = false;
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: StreamBuilder(
        stream: Firestore.instance.collection("ticket").where("uid",isEqualTo: this.widget.uid).where("status", isEqualTo: "open").snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.documents.length > 0) {
            var data = snapshot.data.documents[0];
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(20),
                      child: Image.asset('assets/images/inprogress.png'),
                    ),
                    RaisedButton(
                      color: Colors.red,
                      onPressed: () async {
                        await Firestore.instance.collection("ticket").document(snapshot.data.documents[0].documentID).updateData({
                          'status' :'close',
                          'statusProgress' : 'ลูกค้ายกเลิกการเข้ารับบริการ',
                          'updateDate' : DateTime.now()
                        });
                        await showDialog(
                          context: context,
                          builder: (context) {
                            return Alert(
                              alertType: AlertType.correct,
                              title: "สำเร็จ",
                              message: "ยกเลิกแจ้งซ่อมเรียบร้อยแล้ว",
                              btnText: 'ตกลง',
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            );
                          }
                        );
                      },
                      child: Text(
                        'ยกเลิก !!',
                        style: TextStyle(
                          color: Colors.white
                        ),
                      ),
                    ),
                    Text(
                      "หมายเลขคำร้อง : " + data['caseID'].toString(),
                      style: TextStyle(
                        fontWeight: FontWeight.bold
                      ),
                    ),
                    Text(
                      "สถานะ : " + data['statusProgress'],
                      style: TextStyle(
                        fontWeight: FontWeight.bold
                      ),
                    ),
                    Text(
                      "ช่างที่ได้รับมอบหมาย : " + data['technician'],
                      style: TextStyle(
                        fontWeight: FontWeight.bold
                      ),
                    ),
                    if (data['statusProgress'] == 'ลูกค้านำรถเข้ามาซ่อมที่ศูนย์')
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: Column(
                        children: <Widget>[
                            fileName == '' ? RaisedButton(
                              color: Theme.of(context).primaryColor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              onPressed: () async {
                                file = await FilePicker.getFile(type: FileType.image);
                                if (file != null) {
                                  setState(() {
                                    fileName = p.basename(file.path);
                                  });
                                }
                              },
                              child: Text("Upload"),
                            ) : Column(
                              children: <Widget>[
                                Center(child: Image.file(file)),
                                RaisedButton(
                                  color: Theme.of(context).primaryColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                  ),
                                  onPressed: () async {
                                    var backupfile = file;
                                    file = await FilePicker.getFile(type: FileType.image);
                                    if (file != null) {
                                      setState(() {
                                        fileName = p.basename(file.path);
                                      });
                                    } else {
                                      setState(() {
                                        file = backupfile;
                                      });
                                    }
                                  },
                                  child: Text("Edit"),
                                )
                              ],
                            ),
                          if (fileName != '')
                          Padding(
                            padding: EdgeInsets.only(top: 20),
                            child: createForm ? CustomLoading() : RaisedButton(
                              color: Theme.of(context).primaryColor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              onPressed: () async {
                                setState(() {
                                  createForm = true;
                                });
                                StorageReference storageReference;
                                storageReference = FirebaseStorage.instance.ref().child("ใบเสร็จ/$fileName");
                                final StorageUploadTask uploadTask = storageReference.putFile(file);
                                final StorageTaskSnapshot downloadUrl = (await uploadTask.onComplete);
                                final String url = (await downloadUrl.ref.getDownloadURL());
                                Firestore.instance.collection("ticket").document(data.documentID).updateData({
                                  'receipt' : url,
                                  'statusProgress' : 'รอการตรวจสอบใบเสร็จ',
                                  'updateDate' : DateTime.now(),
                                });
                                setState(() {
                                  createForm = false;
                                });
                                await showDialog(
                                  context: context,
                                  builder: (context) {
                                    return Alert(
                                      alertType: AlertType.correct,
                                      title: "สำเร็จ",
                                      message: "แก้ไขรายละเอียดการแจ้งซ่อมเรียบร้อยแล้ว",
                                      btnText: 'ตกลง',
                                      onPressed: () {
                                        Navigator.of(context).pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
                                      },
                                    );
                                  }
                                );
                              },
                              child: Text("Upload"),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            } else {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 20,right: 20),
                      child: Image.asset('assets/images/helpme.jpg')
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: RaisedButton(
                        child: Text("รับบริการช่วยเหลือ"),
                        color: Theme.of(context).primaryColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        onPressed: (){
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => FormHelp()));
                        },
                      ),
                    ),
                  ],
                ),
              );
            }
          } else {
            return Center(
              child: CustomLoading(),
            );
          }
        }
      ),
    );
  }
}