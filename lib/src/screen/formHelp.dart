import 'package:benelli/src/screen/home.dart';
import 'package:benelli/src/widget/alert.dart';
import 'package:benelli/src/widget/customLoading.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FormHelp extends StatefulWidget {

  @override
  _FormHelpState createState() => _FormHelpState();
}

class _FormHelpState extends State<FormHelp> {
  static GlobalKey<FormState> _formKey;
  TextEditingController _dateController = TextEditingController();
  TextEditingController _timeController = TextEditingController();
  String _model, _problem, _tell, _name,tech,dateTime,time;
  bool createForm = false;
  var item = [
    'Benelli 302R',
    'Benelli 302S',
    'Benelli BN 600i',
    'Benelli Imperiale',
    'Benelli Leoncino',
    'Benelli TNT',
    'Benelli TRE 1130 K',
    'Benelli TRK',
    'Benelli​ TRK502'
  ];
  String uid;
  Future<Null> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      uid = prefs.getString('uid');
    });
  }

  @override
  void initState() { 
    getToken();
    _formKey = GlobalKey<FormState>();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text("ฟอร์มกรอกข้อมูล"),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.only(left: 20,right: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 20),
                  Image.asset('assets/images/fix.jpg'),
                  SizedBox(height: 10),
                  Text(
                    "ชื่อที่ใช้ในการติดต่อ",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).primaryColor
                    ),
                  ),
                  TextFormField(
                    onSaved: (String value) {
                      _name = value;
                    },
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'กรุณากรอกข้อมูล';
                      }
                      return null;
                    },
                  ),
                  Text(
                    "รุ่นของรถ",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).primaryColor
                    ),
                  ),
                  DropdownButtonFormField(
                    value: _model,
                    items: item.map((label) => DropdownMenuItem(
                      child: Text(label.toString()),
                      value: label,
                    )).toList(),
                    onChanged: (value) {
                      _model = value;
                    },
                    validator: (value) {
                      if (value == null) {
                        return 'กรุณากรอกข้อมูล';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 10),
                  Text('อาการที่พบ',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).primaryColor
                    ),
                  ),
                  TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'กรุณากรอกข้อมูล';
                      }
                      return null;
                    },
                    onSaved: (String value) {
                      _problem = value;
                    },
                  ),
                  SizedBox(height: 10),
                  Text("เบอร์โทรศัพท์ติดต่อกลับ",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).primaryColor
                    )
                  ),
                  TextFormField(
                    keyboardType: TextInputType.phone,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'กรุณากรอกข้อมูล';
                      }
                      if (value.isNotEmpty) {
                        if (value.length != 10) {
                          return 'กรุณาตรวจสอบเบอร์โทรใหม่อีกครั้ง';
                        }
                      }
                      return null;
                    },
                    onSaved: (String value) {
                      _tell = value;
                    },
                  ),
                  SizedBox(height: 10),
                  Text(
                    "วันที่ลูกค้าต้องการเข้ารับบริการ",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).primaryColor
                    ),
                  ),
                  TextFormField(
                    controller: _dateController,
                    keyboardType: TextInputType.phone,
                    onTap: () async {
                      FocusScope.of(context).requestFocus(FocusNode());
                      DateTime newDateTime = await showRoundedDatePicker(
                        theme: ThemeData(
                          primaryColor: Theme.of(context).primaryColor,
                          accentColor: Theme.of(context).primaryColor
                        ),
                        context: context,
                        era: EraMode.BUDDHIST_YEAR,
                        initialDate: DateTime.now(),
                        firstDate: DateTime.now().subtract(Duration(days: 3)),
                        lastDate: DateTime.now().add(Duration(days: 30)),
                      );
                      if (newDateTime != null) {
                        _dateController.text = formatDate(newDateTime, [dd,'/', mm,'/', yyyy]).toString();
                      }
                    },
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'กรุณากรอกข้อมูล';
                      }
                      return null;
                    },
                    onSaved: (String value) {
                      dateTime = value;
                    },
                  ),
                  SizedBox(height: 10),
                  Text(
                    "เวลาที่ลูกค้าต้องการเข้ารับบริการ",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).primaryColor
                    ),
                  ),
                  TextFormField(
                    controller: _timeController,
                    keyboardType: TextInputType.phone,
                    onTap: () async {
                      FocusScope.of(context).requestFocus(FocusNode());
                      TimeOfDay timePicked = await showTimePicker(
                        context: context,
                        initialTime: TimeOfDay.now(),
                      );
                      if (timePicked != null) {
                        _timeController.text = timePicked.hour.toString() + " : " + timePicked.minute.toString();
                      }
                    },
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'กรุณากรอกข้อมูล';
                      }
                      return null;
                    },
                    onSaved: (String value) {
                      time = value;
                    },
                  ),
                  SizedBox(height: 10),
                  Text(
                    "ช่างประจำ",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).primaryColor
                    ),
                  ),
                  StreamBuilder<QuerySnapshot>(
                    stream : Firestore.instance.collection("users").where('role',isEqualTo: 'Technician').snapshots(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return Container(
                          child: DropdownButton(
                            value: tech,
                            onChanged: (String newValue) {
                              setState(() {
                                tech = newValue;
                              });
                            },
                            items: snapshot.data.documents.map((DocumentSnapshot document) {
                              return DropdownMenuItem<String>(
                                value: document.data['name'],
                                child: Text(
                                  document.data['name']
                                ),
                              );
                            }).toList(),
                          ),
                        );
                      }
                      return CustomLoading();
                    },
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10,bottom: 10),
                    child: Center(
                      child: createForm == true ? CustomLoading() : RaisedButton(
                        color: Theme.of(context).primaryColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        child: Text('OK'),
                        onPressed: () async {
                          if (_formKey.currentState.validate()) {
                            SharedPreferences prefs = await SharedPreferences.getInstance();
                            _formKey.currentState.save();
                            setState(() {
                              createForm = true;
                            });
                            var respectsQuery = Firestore.instance.collection('ticket');
                            var querySnapshot = await respectsQuery.getDocuments();
                            var totalEquals = querySnapshot.documents.length;
                            Firestore.instance.collection("ticket").document().setData({
                              'caseID' : totalEquals + 1,
                              'name' : _name,
                              'model' : _model,
                              'userPhoto' : prefs.getString("image"),
                              'problem' : _problem,
                              'tel' : _tell,
                              'createDate' : DateTime.now(),
                              'updateDate' : DateTime.now(),
                              'uid' : uid,
                              'status' : 'open',
                              'statusProgress' : "รอการติดต่อกลับจาก Admin",
                              'technician' : 'ว่าง',
                              'dateTime' : dateTime,
                              'time' : time,
                              'requestTechnician' : tech
                            });
                            setState(() {
                              createForm = false;
                            });
                            await showDialog(
                              context: context,
                              builder: (context) {
                                return Alert(
                                  alertType: AlertType.correct,
                                  title: "ระบบได้รับคำร้องเรียบร้อย",
                                  message: "กรุณารอการติดต่อกลับ",
                                  btnText: 'ตกลง',
                                  onPressed: () {
                                    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => Home()), (Route<dynamic> route) => false);
                                  },
                                );
                              }
                            );
                          }
                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}