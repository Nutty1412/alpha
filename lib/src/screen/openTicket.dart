import 'package:benelli/src/screen/detailTicket.dart';
import 'package:benelli/src/widget/customLoading.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class OpenTicket extends StatefulWidget {

  @override
  _OpenTicketState createState() => _OpenTicketState();
}

class _OpenTicketState extends State<OpenTicket> with AutomaticKeepAliveClientMixin<OpenTicket>{
  bool isExpanded = true;
  bool isExpanded2 = true;
  bool isExpanded3 = true;
  @override
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return ListView(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey,
            ),
            borderRadius: BorderRadius.circular(10)
          ),
          child: ExpansionTile(
            initiallyExpanded: true,
            trailing: Icon(isExpanded ? Icons.expand_less : Icons.expand_more,color: isExpanded ? Theme.of(context).primaryColor : Colors.black),
            onExpansionChanged: (bool expanding) => setState(() => this.isExpanded = expanding),
            title: Text(
              "รอการติดต่อกลับจาก Admin",
              style: TextStyle(
                color: isExpanded ? Theme.of(context).primaryColor : Colors.black,
                fontWeight: FontWeight.bold
              ),
            ),
            children: <Widget>[
              StreamBuilder(
                stream : Firestore.instance.collection("ticket").where('status',isEqualTo: 'open').where('statusProgress',isEqualTo: 'รอการติดต่อกลับจาก Admin').snapshots(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data.documents.length == 0) {
                      return Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Text("ไม่มีข้อมูล"),
                      );
                    }
                    return Column(
                      children: List.generate(snapshot.data.documents.length,(i){
                        return Padding(
                          padding: EdgeInsets.only(bottom: 10,left: 10,right: 10),
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.grey[300],
                              ),
                              borderRadius: BorderRadius.circular(10)
                            ),
                            child: ListTile(
                              onTap: (){
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailTicket(
                                  documendID: snapshot.data.documents[i].documentID
                                )));
                              },
                              leading: CircleAvatar(
                                backgroundImage: NetworkImage(snapshot.data.documents[i]['userPhoto']),
                              ),
                              title: Text(snapshot.data.documents[i]['model']),
                              subtitle: Text(snapshot.data.documents[i]['statusProgress']),
                              trailing: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("คุณ : " + snapshot.data.documents[i]['name']),
                                  Text(snapshot.data.documents[i]['tel']),
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                    );
                  } else {
                    return Column(
                      children: <Widget>[
                        CustomLoading(),
                      ],
                    );
                  }
                }
              ),
            ],
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey,
            ),
            borderRadius: BorderRadius.circular(10)
          ),
          child: ExpansionTile(
            initiallyExpanded: true,
            trailing: Icon(isExpanded2 ? Icons.expand_less : Icons.expand_more,color: isExpanded2 ? Theme.of(context).primaryColor : Colors.black),
            onExpansionChanged: (bool expanding) => setState(() => this.isExpanded2 = expanding),
            title: Text(
              "ลูกค้ายืนยันการเข้ารับบริการ",
              style: TextStyle(
                color: isExpanded2 ? Theme.of(context).primaryColor : Colors.black,
                fontWeight: FontWeight.bold
              ),
            ),
            children: <Widget>[
              StreamBuilder(
                stream : Firestore.instance.collection("ticket").where('status',isEqualTo: 'open').where('statusProgress',isEqualTo: 'ลูกค้ายืนยันการเข้ารับบริการ').snapshots(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data.documents.length == 0) {
                      return Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Text("ไม่มีข้อมูล"),
                      );
                    }
                    return Column(
                      children: List.generate(snapshot.data.documents.length,(i){
                        return Padding(
                          padding: EdgeInsets.only(bottom: 10,left: 10,right: 10),
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.grey[300],
                              ),
                              borderRadius: BorderRadius.circular(10)
                            ),
                            child: ListTile(
                              onTap: (){
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailTicket(
                                  documendID: snapshot.data.documents[i].documentID
                                )));
                              },
                              leading: CircleAvatar(
                                backgroundImage: NetworkImage(snapshot.data.documents[i]['userPhoto']),
                              ),
                              title: Text(snapshot.data.documents[i]['model']),
                              subtitle: Text(snapshot.data.documents[i]['statusProgress']),
                              trailing: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("คุณ : " + snapshot.data.documents[i]['name']),
                                  Text(snapshot.data.documents[i]['tel']),
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                    );
                  } else {
                    return Column(
                      children: <Widget>[
                        CustomLoading(),
                      ],
                    );
                  }
                }
              ),
            ],
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey,
            ),
            borderRadius: BorderRadius.circular(10)
          ),
          child: ExpansionTile(
            initiallyExpanded: true,
            trailing: Icon(isExpanded3 ? Icons.expand_less : Icons.expand_more,color: isExpanded3 ? Theme.of(context).primaryColor : Colors.black),
            onExpansionChanged: (bool expanding) => setState(() => this.isExpanded3 = expanding),
            title: Text(
              "ลูกค้านำรถเข้ามาซ่อมที่ศูนย์",
              style: TextStyle(
                color: isExpanded3 ? Theme.of(context).primaryColor : Colors.black,
                fontWeight: FontWeight.bold
              ),
            ),
            children: <Widget>[
              StreamBuilder(
                stream : Firestore.instance.collection("ticket").where('status',isEqualTo: 'open').where('statusProgress',isEqualTo: 'ลูกค้านำรถเข้ามาซ่อมที่ศูนย์').snapshots(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data.documents.length == 0) {
                      return Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Text("ไม่มีข้อมูล"),
                      );
                    }
                    return Column(
                      children: List.generate(snapshot.data.documents.length,(i){
                        return Padding(
                          padding: EdgeInsets.only(bottom: 10,left: 10,right: 10),
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.grey[300],
                              ),
                              borderRadius: BorderRadius.circular(10)
                            ),
                            child: ListTile(
                              onTap: (){
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailTicket(
                                  documendID: snapshot.data.documents[i].documentID
                                )));
                              },
                              leading: CircleAvatar(
                                backgroundImage: NetworkImage(snapshot.data.documents[i]['userPhoto']),
                              ),
                              title: Text(snapshot.data.documents[i]['model']),
                              subtitle: Text(snapshot.data.documents[i]['statusProgress']),
                              trailing: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("คุณ : " + snapshot.data.documents[i]['name']),
                                  Text(snapshot.data.documents[i]['tel']),
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                    );
                  } else {
                    return Column(
                      children: <Widget>[
                        CustomLoading(),
                      ],
                    );
                  }
                }
              ),
            ],
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey,
            ),
            borderRadius: BorderRadius.circular(10)
          ),
          child: ExpansionTile(
            initiallyExpanded: true,
            trailing: Icon(isExpanded3 ? Icons.expand_less : Icons.expand_more,color: isExpanded3 ? Theme.of(context).primaryColor : Colors.black),
            onExpansionChanged: (bool expanding) => setState(() => this.isExpanded3 = expanding),
            title: Text(
              "รอการตรวจสอบใบเสร็จ",
              style: TextStyle(
                color: isExpanded3 ? Theme.of(context).primaryColor : Colors.black,
                fontWeight: FontWeight.bold
              ),
            ),
            children: <Widget>[
              StreamBuilder(
                stream : Firestore.instance.collection("ticket").where('status',isEqualTo: 'open').where('statusProgress',isEqualTo: 'รอการตรวจสอบใบเสร็จ').snapshots(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data.documents.length == 0) {
                      return Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Text("ไม่มีข้อมูล"),
                      );
                    }
                    return Column(
                      children: List.generate(snapshot.data.documents.length,(i){
                        return Padding(
                          padding: EdgeInsets.only(bottom: 10,left: 10,right: 10),
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.grey[300],
                              ),
                              borderRadius: BorderRadius.circular(10)
                            ),
                            child: Column(
                              children: [
                                ListTile(
                                  onTap: (){
                                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailTicket(
                                      documendID: snapshot.data.documents[i].documentID
                                    )));
                                  },
                                  leading: CircleAvatar(
                                    backgroundImage: NetworkImage(snapshot.data.documents[i]['userPhoto']),
                                  ),
                                  title: Text(snapshot.data.documents[i]['model']),
                                  subtitle: Text(snapshot.data.documents[i]['statusProgress']),
                                  trailing: Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text("คุณ : " + snapshot.data.documents[i]['name']),
                                      Text(snapshot.data.documents[i]['tel']),
                                      Text('ค่าซ่อม : ' + snapshot.data.documents[i]['price'])
                                    ],
                                  ),
                                ),
                                ListTile(
                                  leading: IconButton(icon: Icon(Icons.check), onPressed: () async {
                                    await Firestore.instance.collection("ticket").document(snapshot.data.documents[i].documentID).updateData({
                                      'statusProgress' : 'ดำเนินการซ่อมเสร็จสิ้น',
                                      'status' : 'close' 
                                    });
                                  }),
                                  trailing: IconButton(icon: Icon(Icons.close), onPressed: () async {
                                    await Firestore.instance.collection("ticket").document(snapshot.data.documents[i].documentID).updateData({
                                      'statusProgress' : 'ลูกค้านำรถเข้ามาซ่อมที่ศูนย์'
                                    });
                                  }),
                                  title: Image.network(snapshot.data.documents[i]['receipt'])
                                ),
                              ]
                            ),
                          ),
                        );
                      }),
                    );
                  } else {
                    return Column(
                      children: <Widget>[
                        CustomLoading(),
                      ],
                    );
                  }
                }
              ),
            ],
          ),
        ),
      ],
    );
  }
}