import 'package:benelli/src/screen/closeTicket.dart';
import 'package:benelli/src/screen/openTicket.dart';
import 'package:flutter/material.dart';

class ScreenAdmin extends StatefulWidget {
  @override
  _ScreenAdminState createState() => _ScreenAdminState();
}

class _ScreenAdminState extends State<ScreenAdmin> with TickerProviderStateMixin {
  TabController _customTabController;

  @override
  void initState() {
    super.initState();
    _customTabController = new TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _customTabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        TabBar(
          controller: _customTabController,
          indicatorColor: Theme.of(context).primaryColor,
          labelColor: Theme.of(context).primaryColor,
          unselectedLabelColor: Colors.black54,
          isScrollable: true,
          tabs: <Widget>[
            Tab(
              text: "รายการที่ดำเนินการอยู่",
            ),
            Tab(
              text: "รายการที่เสร็จสิ้นแล้ว",
            ),
          ],
        ),
        Container(
          height: screenHeight * 0.70,
          margin: EdgeInsets.only(left: 16.0, right: 16.0),
          child: TabBarView(
            controller: _customTabController,
            children: <Widget>[
              OpenTicket(),
              CloseTicket()
            ],
          ),
        )
      ],
    );
  }
}