import 'package:benelli/src/screen/editTicket.dart';
import 'package:benelli/src/widget/customLoading.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';

import 'editsTicket.dart';

class DetailTicket extends StatefulWidget {
  final documendID;
  DetailTicket({
    @required this.documendID
  });

  @override
  _DetailTicketState createState() => _DetailTicketState();
}

class _DetailTicketState extends State<DetailTicket> {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("รายละเอียดการแจ้งซ่อม"),
      ),
      body: SingleChildScrollView(
        child: StreamBuilder(
          stream: Firestore.instance.collection("ticket").document(this.widget.documendID).snapshots(),
          builder: (context,snapshot) {
            if (snapshot.hasData) {
              var caseID = snapshot.data;
              return Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                          color: Colors.grey
                        )
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            detail("หมายเลขแจ้งซ่อม",caseID['caseID'].toString()),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  width: (MediaQuery.of(context).size.width-40) / 2,
                                  child: detail("เวลา / วันที่รับเรื่อง",formatDate(caseID['createDate'].toDate(), [HH, ':', nn,' ', '/',' ', dd,'/', mm,'/', yyyy]).toString())
                                ),
                                detail("เวลา / วันที่อัพเดต",formatDate(caseID['updateDate'].toDate(), [HH, ':', nn,' ', '/',' ', dd,'/', mm,'/', yyyy]).toString())
                              ],
                            ),
                            detail("เวลา / วันที่ลูกค้าต้องการนำรถเข้าศูนย์",caseID['time'] + ' / ' + caseID['dateTime'].toString()),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: Colors.grey
                          )
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  CircleAvatar(
                                    backgroundImage: NetworkImage(caseID['userPhoto']),
                                    radius: 20,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  detail("ชื่อลูกค้า",caseID['name']),
                                ],
                              ),
                              detail("เบอร์โทรศัพท์",caseID['tel'])
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: Colors.grey
                          )
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              detail("รุ่นของรถ",caseID['model']),
                              detail("ปัญหาที่เจอ",caseID['problem']),
                              detail("ช่างที่ต้องการ",caseID['requestTechnician'])
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: Colors.grey
                          )
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              detail("สถานะดำเนินการ",caseID['statusProgress']),
                            ],
                          ),
                        ),
                      ),
                    ),
                    if (caseID['technician'] != 'ว่าง')
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: Colors.grey
                          )
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              detail("ช่างที่ได้รับมอบหมาย",caseID['technician']),
                              Row(
                                children: <Widget>[
                                  Container(
                                    width: (MediaQuery.of(context).size.width - 40) /2 ,
                                    child: detail("เวลาที่นัดหมาย",caseID['time'])
                                  ),
                                  detail("วันที่นัดหมาย",caseID['dateTime']),
                                ],
                              ),
                              detail("รายละเอียดการซ่อม", caseID['details'])
                            ],
                          ),
                        ),
                      ),
                    ),
                    if(caseID['status'] == 'open')
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: RaisedButton(
                        onPressed: (){
                          if (caseID['statusProgress'] != 'รอการติดต่อกลับจาก Admin') {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => EditsTicket(
                              caseID: caseID,
                              documendID: this.widget.documendID,
                            )));
                          } else {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => EditTicket(
                              caseID: caseID,
                              documendID: this.widget.documendID,
                            )));
                          }
                        },
                        color: Theme.of(context).primaryColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                        child: Text('แก้ไขข้อมูล'),
                      ),
                    )
                  ],
                ),
              );
            }
            return Padding(
              padding: EdgeInsets.only(top: 20),
              child: Column(
                children: <Widget>[
                  CustomLoading(),
                ],
              ),
            );
          }
        ),
      )
    );
  }
  Widget detail(title,subTitle){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(title,style: TextStyle(fontWeight: FontWeight.bold,color: Theme.of(context).primaryColor),),
        Text(subTitle)
      ],
    );
  }
}