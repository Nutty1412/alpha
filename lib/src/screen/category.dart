import 'package:benelli/src/screen/product.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../categoryMock.dart';

class Catagory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ImageSlideshow(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * 0.3,
          initialPage: 0,
          indicatorColor: Colors.blue,
          indicatorBackgroundColor: Colors.grey,
          children: List.generate(
            CategoryMock().catagory.length,(index) {
              var data = CategoryMock().catagory[index];
              return Stack(
                fit: StackFit.expand,
                children: [
                  Image.asset(
                    'assets/images/${data['picture']}.png',
                    fit: BoxFit.cover,
                  ),
                  Center(
                    child: RaisedButton(
                      color: Colors.orange,
                      child: Text(
                        data['category'],
                        style: TextStyle(
                          color: Colors.white
                        ),
                      ),
                      onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => Product(category: data['category'],)));
                      }
                    ),
                  )
                ],
              );
            }
          ),
          autoPlayInterval: 5000,
        ),
        Image.asset('assets/images/logo.png'),
        Text('ที่อยู่: 17/7-12หมู่8 อ้อมใหญ่ สามพราน นครปฐม 73160'),
        SizedBox(height: 5),
        Text('เวลาทำการ: เปิด ⋅ ปิด 17:30'),
        SizedBox(height: 5),
        IconButton(
          color: Colors.blue,
          icon: Icon(Icons.map),
          onPressed: () async {
            launch('https://www.google.com/maps/place/Benelli+%E0%B8%99%E0%B8%84%E0%B8%A3%E0%B8%9B%E0%B8%90%E0%B8%A1/@13.7080896,100.2777891,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2955cffd124cf:0xa2028fee4f8bfa24!8m2!3d13.7080896!4d100.2799778');
          }
        )
      ],
    );
  }
}