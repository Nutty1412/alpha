import 'package:benelli/src/App.dart';
import 'package:benelli/src/controllers/signIn.dart';
import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class HomeNotLogin extends StatefulWidget {

  @override
  _HomeNotLoginState createState() => _HomeNotLoginState();
}

class _HomeNotLoginState extends State<HomeNotLogin> {
  YoutubePlayerController _controller = YoutubePlayerController(
    initialVideoId: 'wuhUdGT_W5E',
    flags: YoutubePlayerFlags(
      autoPlay: true,
      mute: true,
      loop: true
    ),
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Benelli'),
      ),
      body: Column(
        children: [
          YoutubePlayer(
            controller: _controller,
            showVideoProgressIndicator: true
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 40),
            child: Image.asset('assets/images/logo.png'),
          ),
          SignInButton(
            Buttons.GoogleDark,
            text: "Sign in with Google",
            onPressed: () async {
              Auth().signInWithGoogle().then((result) {
                if (result != null) {
                  openHome(context);
                } else {
                  print('Cancel');
                }
              });
            },
          )
        ],
      ),
    );
  }

    openHome(BuildContext context) async {
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => App()), (Route<dynamic> route) => false);
  }
}