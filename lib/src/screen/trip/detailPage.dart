import 'dart:convert';

import 'package:benelli/src/screen/trip/registerTrip.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class DetailPage extends StatelessWidget {
  final documentID;

  DetailPage({
    @required this.documentID
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ทริปท่องเที่ยว'),
        centerTitle: true
      ),
      body: StreamBuilder(
        stream: Firestore.instance.collection('trip').document(this.documentID).snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            var data = snapshot.data;
            var limit = data['limit'] - data['people'].length;
            return SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.memory(base64Decode(data['image'].toString())),
                    SizedBox(height: 10),
                    Text(
                      data['title'],
                      style: TextStyle(
                        fontWeight: FontWeight.bold
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(data['subTitle']),
                    Divider(
                      color: Colors.black87,
                    ),
                    Text(
                      'ค่าเข้าทริป ${data['price'].toString()} บาท',
                      style: TextStyle(
                        fontWeight: FontWeight.bold
                      )
                    ),
                    Text(
                      'เหลืออีก $limit คน',
                      style: TextStyle(
                        fontWeight: FontWeight.bold
                      ),
                    ),
                    // Divider(
                    //   color: Colors.black87,
                    // ),
                    // Text(
                    //   'รายชื่อสมาชิกที่เข้าร่วม',
                    //   style: TextStyle(
                    //     fontWeight: FontWeight.bold
                    //   ),
                    // ),
                    // data['people'].length == 0 ? Text('ยังไม่มีสมาชิกที่เข้าร่วม') : Text('data'),
                    // Divider(
                    //   color: Colors.black87,
                    // ),
                    if (data['status'] == 'open')
                    RaisedButton(
                      color: Theme.of(context).primaryColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      child: Text('สมัครทริป'),
                      onPressed: () async {
                        await Navigator.push(context, MaterialPageRoute(builder: (context) => RegisterTrip(
                          nameTrip: data['title'],
                          documentID: documentID,
                          tripBath: data['price'],
                        ))).then((value) {
                          if (value == 'Success') {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text("จองทริปสำเร็จแล้ว โปรดรอการตรวจสอบจากแอดมิน"),
                            ));
                          }
                        });
                      }
                    )
                  ],
                ),
              ),
            );
          }
          return Center(child: CircularProgressIndicator());
        },
      )
    );
  }
}