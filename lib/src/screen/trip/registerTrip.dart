import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as p;
import 'package:shared_preferences/shared_preferences.dart';

class RegisterTrip extends StatefulWidget {

  final documentID,tripBath,nameTrip;

  RegisterTrip({
    @required this.nameTrip,
    @required this.documentID,
    @required this.tripBath
  });

  @override
  _RegisterTripState createState() => _RegisterTripState();
}

class _RegisterTripState extends State<RegisterTrip> {
  static GlobalKey<FormState> _formKey;
  String name,details,phone,emergencyCall,model;
  File file;
  String fileName = '';
  bool loading = false;
  var item = [
    'Benelli 302R',
    'Benelli 302S',
    'Benelli BN 600i',
    'Benelli Imperiale',
    'Benelli Leoncino',
    'Benelli TNT',
    'Benelli TRE 1130 K',
    'Benelli TRK',
    'Benelli​ TRK502'
  ];

  @override
  void initState() {
    _formKey = GlobalKey<FormState>();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('สมัครทริป'),
        centerTitle: true,
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                container(
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      detail("ชื่อ - นามสกุล", TextFormField(
                        onSaved: (String value) {
                          name = value;
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'กรุณากรอกข้อมูล';
                          }
                          return null;
                        },
                      )),
                      detail("เบอร์ติดต่อ", TextFormField(
                        onSaved: (String value) {
                          phone = value;
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'กรุณากรอกข้อมูล';
                          }
                          return null;
                        },
                      )),
                      detail("เบอร์ติดต่อฉุกเฉิน", TextFormField(
                        onSaved: (String value) {
                          emergencyCall = value;
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'กรุณากรอกข้อมูล';
                          }
                          return null;
                        },
                      )),
                      Text(
                        "รุ่นของรถ",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                      DropdownButtonFormField(
                        value: model,
                        items: item.map((label) => DropdownMenuItem(
                          child: Text(label.toString()),
                          value: label,
                        )).toList(),
                        onChanged: (value) {
                          model = value;
                        },
                        validator: (value) {
                          if (value == null) {
                            return 'กรุณากรอกข้อมูล';
                          }
                          return null;
                        },
                      ),
                      detail("รายละเอียด", TextFormField(
                        onSaved: (String value) {
                          details = value;
                        },
                      ))
                    ],
                  )
                ),
                SizedBox(height: 20),
                Text('ค่าใช้จ่ายในทริปนี้ ${this.widget.tripBath.toString()} บาท (ต่อ 1 คน)'),
                Text('โอนเพื่อจองได้ที่ บัญชี xxx-xxx-xxxx ธนาคาร xxxx'),
                fileName == '' ? RaisedButton(
                  color: Theme.of(context).primaryColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                  onPressed: () async {
                    file = await FilePicker.getFile(type: FileType.image);
                    if (file != null) {
                      setState(() {
                        fileName = p.basename(file.path);
                      });
                    }
                  },
                  child: Text("เพิ่มสลิป"),
                ) : Column(
                  children: <Widget>[
                    Center(child: Image.file(file)),
                    RaisedButton(
                      color: Theme.of(context).primaryColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      onPressed: () async {
                        var backupfile = file;
                        file = await FilePicker.getFile(type: FileType.image);
                        if (file != null) {
                          setState(() {
                            fileName = p.basename(file.path);
                          });
                        } else {
                          setState(() {
                            file = backupfile;
                          });
                        }
                      },
                      child: Text("แก้ไข"),
                    ),
                    if (fileName != '')
                      loading ? CircularProgressIndicator() : RaisedButton(
                        color: Theme.of(context).primaryColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        child: Text('จองทริป'),
                        onPressed: () async {
                          setState(() {
                            loading = true;
                          });
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            SharedPreferences prefs = await SharedPreferences.getInstance();
                            StorageReference storageReference;
                            storageReference = FirebaseStorage.instance.ref().child("ใบเสร็จ/$fileName");
                            final StorageUploadTask uploadTask = storageReference.putFile(file);
                            final StorageTaskSnapshot downloadUrl = (await uploadTask.onComplete);
                            final String url = (await downloadUrl.ref.getDownloadURL());
                            await Firestore.instance.collection('registerTrip').add({
                              'nameTrip' : this.widget.nameTrip,
                              'userRegis' : prefs.getString('uid'),
                              'tripRegis' : this.widget.documentID,
                              'name' : name,
                              'details' : details,
                              'phone' : phone,
                              'emergencyCall' : emergencyCall,
                              'model' : model,
                              'imageSlip' : url,
                              'price' : this.widget.tripBath,
                              'status' : 'รอการตรวจสอบ',
                              'process' : 'open'
                            });
                            Navigator.pop(context,'Success');
                          }
                        }
                      )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget detail(title,Widget subTitle){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(title,style: TextStyle(fontWeight: FontWeight.bold,color: Theme.of(context).primaryColor)),
        subTitle
      ],
    );
  }

  Widget container(Widget child) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(
          color: Colors.grey
        )
      ),
      child: Padding(
        padding: EdgeInsets.all(10),
        child : child
      )
    );
  }
}