import 'package:benelli/src/screen/screenAdmin.dart';
import 'package:benelli/src/screen/screenTechnician.dart';
import 'package:benelli/src/screen/screenUser.dart';
import 'package:benelli/src/screen/trip/tripAdminPage.dart';
import 'package:benelli/src/screen/tripUserPage.dart';
import 'package:benelli/src/widget/customLoading.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ManageTrip extends StatefulWidget {
  @override
  _ManageTripState createState() => _ManageTripState();
}

class _ManageTripState extends State<ManageTrip> {
  String uid,role,name;
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getToken(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          return role == 'Member' ? TripUserPage() : TripAdminPage();
        } else {
          return Center(
            child: CustomLoading(),
          );
        }
      }
    );
  }

  getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      uid = prefs.getString('uid');
      role = prefs.getString('role');
      name = prefs.getString('name');
    });

    return role;
  }
}