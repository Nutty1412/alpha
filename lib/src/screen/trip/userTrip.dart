import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserTrip extends StatefulWidget {
  @override
  _UserTripState createState() => _UserTripState();
}

class _UserTripState extends State<UserTrip> {
  String uid;
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getToken(),
      builder: (BuildContext context , AsyncSnapshot snap){
        if (snap.data != null) {
          return StreamBuilder(
            stream: Firestore.instance.collection('registerTrip').where('userRegis',isEqualTo: uid).snapshots(),
            builder: (BuildContext context , AsyncSnapshot snapshot){
              if (snapshot.data != null) {
                return ListView.separated(
                  separatorBuilder: (context, index) => Divider(),
                  itemCount: snapshot.data.documents.length,
                  itemBuilder: (BuildContext ctx, int index) {
                    var data = snapshot.data.documents[index];
                    return Card(
                      child: ListTile(
                        // onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => DetailPage(documentID: data.documentID))),
                        leading: data['status'] == 'รอการตรวจสอบ' ? Icon(Icons.timelapse,color: Colors.orange) : data['status'] == 'ยืนยัน' ? Icon(Icons.check,color: Colors.green) : Icon(Icons.cancel,color: Colors.red),
                        title: Text(data['nameTrip']),
                        subtitle: Text(
                          'สถานะ : ${data['status']}',
                          maxLines: 2,
                        ),
                      ),
                    );
                  }
                );
              }
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          );
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }

  getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    uid = prefs.getString('uid');
    return prefs.getString('uid');
  }
}