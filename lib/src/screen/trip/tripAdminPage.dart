import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class TripAdminPage extends StatefulWidget {
  @override
  _TripAdminPageState createState() => _TripAdminPageState();
}

class _TripAdminPageState extends State<TripAdminPage>  with TickerProviderStateMixin {
  TabController _customTabController;

  @override
  void initState() {
    super.initState();
    _customTabController = new TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _customTabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        TabBar(
          controller: _customTabController,
          indicatorColor: Theme.of(context).primaryColor,
          labelColor: Theme.of(context).primaryColor,
          unselectedLabelColor: Colors.black54,
          isScrollable: true,
          tabs: <Widget>[
            Tab(
              text: "รายการที่รอการตรวจสอบ",
            ),
            Tab(
              text: "รายการที่ตรวจสอบแล้ว",
            ),
          ],
        ),
        Container(
          height: screenHeight * 0.70,
          margin: EdgeInsets.only(left: 16.0, right: 16.0),
          child: TabBarView(
            controller: _customTabController,
            children: <Widget>[
              StreamBuilder(
                stream: Firestore.instance.collection('registerTrip').where('process',isEqualTo: 'open').snapshots(),
                builder: (BuildContext context , AsyncSnapshot snapshot){
                  if (snapshot.data != null) {
                    return ListView.separated(
                      separatorBuilder: (context, index) => Divider(),
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (BuildContext ctx, int index) {
                        var data = snapshot.data.documents[index];
                        return Card(
                          child: Column(
                            children: [
                              ListTile(
                                // onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => DetailPage(documentID: data.documentID))),
                                leading: data['status'] == 'รอการตรวจสอบ' ? Icon(Icons.timelapse,color: Colors.orange) : data['status'] == 'ยืนยัน' ? Icon(Icons.check,color: Colors.green) : Icon(Icons.cancel,color: Colors.red),
                                title: Text(data['nameTrip']),
                                subtitle: Text(
                                  'สถานะ : ${data['status']}\nค่าใช้จ่าย : ${data['price']}',
                                ),
                              ),
                              Image.network(data['imageSlip']),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: [
                                  IconButton(
                                    icon: Icon(Icons.check),
                                    color: Colors.green,
                                    onPressed: () async {
                                      await Firestore.instance.collection('registerTrip').document(data.documentID).updateData({
                                        'status' : 'ยืนยัน',
                                        'process' : 'close'
                                      });
                                    }
                                  ),
                                  IconButton(
                                    icon: Icon(Icons.close),
                                    color: Colors.red,
                                    onPressed: () async {
                                      await Firestore.instance.collection('registerTrip').document(data.documentID).updateData({
                                        'status' : 'ยกเลิก',
                                        'process' : 'close'
                                      });
                                    }
                                  )
                                ],
                              )
                            ],
                          ),
                        );
                      }
                    );
                  }
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              ),
              StreamBuilder(
                stream: Firestore.instance.collection('registerTrip').where('process',isEqualTo: 'close').snapshots(),
                builder: (BuildContext context , AsyncSnapshot snapshot){
                  if (snapshot.data != null) {
                    return ListView.separated(
                      separatorBuilder: (context, index) => Divider(),
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (BuildContext ctx, int index) {
                        var data = snapshot.data.documents[index];
                        return Card(
                          child: ListTile(
                            // onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => DetailPage(documentID: data.documentID))),
                            leading: data['status'] == 'รอการตรวจสอบ' ? Icon(Icons.timelapse,color: Colors.orange) : data['status'] == 'ยืนยัน' ? Icon(Icons.check,color: Colors.green) : Icon(Icons.cancel,color: Colors.red),
                            title: Text(data['nameTrip']),
                            subtitle: Text(
                              'สถานะ : ${data['status']}',
                              maxLines: 2,
                            ),
                          ),
                        );
                      }
                    );
                  }
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              ),
            ],
          ),
        )
      ],
    );
  }
}