import 'dart:convert';

import 'package:benelli/src/screen/trip/detailPage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class CloseTrip extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: Firestore.instance.collection('trip').where('status',isEqualTo: 'close').snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return ListView.separated(
            separatorBuilder: (context, index) => Divider(),
            itemCount: snapshot.data.documents.length,
            itemBuilder: (BuildContext ctx, int index) {
              var data = snapshot.data.documents[index];
              return Card(
                child: ListTile(
                  onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => DetailPage(documentID: data.documentID))),
                  leading: Image.memory(base64Decode(data['image'].toString())),
                  title: Text(data['title']),
                  subtitle: Text(
                    data['subTitle'],
                    maxLines: 2,
                  ),
                ),
              );
            }
          );
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }
}