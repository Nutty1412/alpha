import 'package:benelli/src/controllers/signIn.dart';
import 'package:benelli/src/screen/home.dart';
import 'package:benelli/src/screen/homeNotLogin.dart';
import 'package:benelli/src/widget/customLoading.dart';
import 'package:flutter/material.dart';

class AuthPage extends StatefulWidget {
  @override
  _AuthPageState createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  String _auth;

  @override
  void initState(){
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_){
      _checkToken();
    });
    
  }

  @override
  Widget build(BuildContext context) {
    switch (_auth) {
      case 'LOGGED_IN':
        return Home();
        break;
      case 'NOT_LOGGED_IN':
        return HomeNotLogin();
        break;
      default: 
        return Scaffold(
          body: Center(
            child: CustomLoading(),
          ),
        );
    }
  }

  _checkToken() async {
    Auth().getCurrentUser().then((user){
      if (user != null) {
        setState(() {
          _auth = 'LOGGED_IN';
        });
      } else {
        setState(() {
          _auth = 'NOT_LOGGED_IN';
        });
      }
    });
  }
}