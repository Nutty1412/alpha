import 'package:benelli/src/models/googleSignin.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Auth {
  Future<FirebaseUser> getCurrentUser() async {
    FirebaseUser user = await Google().getCurrentUser();
    if (user != null) {
      Firestore.instance.collection("users").document(user.uid).get().then((value){
        if (value.data != null) {
          Firestore.instance.collection("users").document(user.uid).updateData({
            'picture' : user.photoUrl,
            'email' : user.email,
          });
        } else {
          Firestore.instance.collection("users").document(user.uid).setData({
            'name' : user.displayName,
            'picture' : user.photoUrl,
            'email' : user.email,
            'role' : "Member",
            'status' : "Free"
          });
        }
      });
      return user;
    }
    return null;
  }

  Future<String> signInWithGoogle() async {
    return await Google().signInWithGoogle();
  }

  Future signOutGoogle() async {
    await Google().signOutGoogle();
  }
}