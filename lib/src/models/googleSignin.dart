import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Google {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn(
    scopes: [
      'email',
      'profile',
      'https://www.googleapis.com/auth/contacts.readonly',
    ],
  );

  Future<String> signInWithGoogle() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
      if (googleSignInAccount != null) {
        final GoogleSignInAuthentication googleSignInAuthentication =
            await googleSignInAccount.authentication;

        final AuthCredential credential = GoogleAuthProvider.getCredential(
          accessToken: googleSignInAuthentication.accessToken,
          idToken: googleSignInAuthentication.idToken,
        );

        final FirebaseUser user = (await _auth.signInWithCredential(credential)).user;
        await user.getIdToken().then((token){
          prefs.setString('token', token.toString());
        });
        return 'LOGGED_IN';
      }
      } catch (e) {
        return null;
      }
    return null;
  }

  Future<FirebaseUser> getCurrentUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    FirebaseUser user = await _auth.currentUser();
    if (user != null){
      user.getIdToken().then((token){
        prefs.setString("uid", user.uid);
        prefs.setString("image", user.photoUrl);
      });
      Firestore.instance.collection("users").document(user.uid).get().then((value){
        prefs.setString("role", value.data['role']);
        prefs.setString("name", value.data['name']);
      });
    }
    return user;
  }

  void signOutGoogle() async{
    await googleSignIn.signOut();
    await _auth.signOut();
  }
}